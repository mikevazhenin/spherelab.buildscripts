$verName = ".ver"

#
# Получение текущей версии
#
function Get-CurrentVersion {
    param ($fileVersionName = $verName)
    
    $currentVersion = [version](Get-Content $fileVersionName | Select-Object -First 1)
    return $currentVersion
}

#
# Получение текущей версии и увеличение minor
# 
function Get-NextMinorVersion {
    #[OutputType([version])]
    param (
        $fileVersionName = $verName       
        )
    $currentVersion = Get-CurrentVersion -fileVersionName $fileVersionName
    $nextVersion = New-Object System.Version("{0}.{1}.{2}" -f $currentVersion.Major, ($currentVersion.Minor + 1), 0)

    return $nextVersion
}

#
# Получение текущей версии и увеличение patch
# 
function Get-NextPatchVersion {
    #[OutputType([version])]
    param ($fileVersionName = $verName)
    $currentVersion = Get-CurrentVersion -fileVersionName $fileVersionName
    $nextVersion = New-Object System.Version("{0}.{1}.{2}" -f $currentVersion.Major, $currentVersion.Minor, ($currentVersion.Build + 1))

    return $nextVersion
}