#
# Поиск ближайшего пути вверх относительно указанной папки
#
function Find-PathToTopRecursively {
    param ([string]$pathName, $searchIn)
    
    if (-Not [System.IO.Directory]::Exists($searchIn)) {      
        return $null
    }
    
    $fullPath = [System.IO.Path]::Combine($searchIn, $pathName)

    
    #$file = Get-ChildItem -Path $searchIn -Include $pathName
      
    if (Test-Path -Path $fullPath) {
        return $fullPath;
    }
    else {
        $parent = (get-item $searchIn).parent.FullName      
        return Find-PathToTopRecursively -pathName $pathName -searchIn $parent
    }
}

function Enter-ProjectRoot {
  param ([string]$path)
    
  if (-Not [System.IO.Directory]::Exists($path)) {      
    throw "Folder $path not found"
  }

  $parentFolder = Split-Path -Path $path -Parent

  $fullPath = [System.IO.Path]::Combine($parentFolder, ".git")

  if (-Not (Test-Path -Path $fullPath)) {
      throw "Folder $parentFolder isn't Git root"        
  }

  Set-Location -Path $parentFolder
}





