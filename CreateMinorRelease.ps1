$scriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
Import-Module (Resolve-Path("$scriptDir\Modules\Paths.psm1"))
Import-Module (Resolve-Path("$scriptDir\Modules\Version.psm1"))

#$gitRoot = Split-Path -Path $scriptDir -Parent
Enter-ProjectRoot -Path $scriptDir

$nextVersion = Get-NextMinorVersion

Write-host "Create new branch 'release-$nextVersion'"

git flow release start -F $nextVersion

Set-Content -Path ".ver" -Value "$nextVersion"
Write-host "File .ver patched with version $nextVersion"

git commit -a -m "Version $nextVersion"
Write-host "New version $nextVersion"

Write-host "Don't forget to push local changes"
$host.UI.RawUI.ReadKey()

